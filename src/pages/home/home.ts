import { StatsPage } from '../stats/stats';
import { ModalController } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { Component } from '@angular/core';
import { Player } from '../../models/player';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  scores: Array<any> = [];
  opts: {
    data:string;
  };

    constructor(private localStorage: LocalStorageProvider,private modalCtrl: ModalController){
      this.opts = {
        data:'scores'
      };
  }
    ionViewWillEnter(){
        this.localStorage.fetchAll(this.opts['data']).then(
        (data) =>  {
// console.log(data);
          this.scores = data ? data : [];
        });
 //   console.log(this.scores);
  }
    view(item){
        let sco = this.modalCtrl.create(StatsPage,{scores:item,players:item['players'],tr:item['trackers']});
    sco.present();
    }
    ionViewWillLeave(){
  
    }

}
