import { AuthService } from '../services/auth.service';
import { InGamePage } from '../pages/start-game/in-game';
import { StatsPage } from '../pages/stats/stats';
import { HomePage } from '../pages/home/home';
import { SetupTabsPage } from '../pages/setup/setup-tabs';
import { StartGamePage } from '../pages/start-game/start-game';
import { ScorePage } from '../pages/score/score';
import { HistoryPage } from '../pages/history/history';
import { SettingsPage } from '../pages/settings/settings';
import { Component, ViewChild } from '@angular/core';
import { MenuController, ModalController, NavController, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from "../pages/login/login";
import firebase from 'firebase';
import { environment } from '../environment/environment.dev';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  InGamePage: InGamePage;


  @ViewChild('nav') nav: NavController;
  //rootPage: any = HomePage;
  //rootPage: any = SetupTabsPage;
 rootPage: any = LoginPage;
  
  pages: Array<{title:string, component:any, icon: string}>;

  constructor(public authService:AuthService, public platform: Platform,private menuCtrl: MenuController,  public statusBar: StatusBar, public splashScreen: SplashScreen) {
      this.pages = [
      { title: 'Home', component: HomePage, icon: 'home' },
      { title: 'Setup', component: SetupTabsPage,  icon: 'clipboard' },
      { title: 'Start game', component: StartGamePage, icon: 'play' },
      { title: 'Scores', component: ScorePage, icon: 'list' },
      { title: 'History', component: HistoryPage, icon: 'time'}//,
    //  { title: 'Settings', component: SettingsPage, icon: 'settings' }
    ];
    firebase.initializeApp(environment.firebase);
    firebase.auth().onAuthStateChanged((user:firebase.User) => {
          if (!user) {
              //console.log("not login");
              this.rootPage = LoginPage;

          } else {
              //console.log("login");
              this.rootPage =  HomePage;
          }

      });
     this.initializeApp();
     
  }
   initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page: any) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
    logout() {
    this.authService.logoutUser();
    this.nav.setRoot(LoginPage);
  }
}

