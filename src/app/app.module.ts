import { environment } from '../environment/environment.dev';
import { AuthService } from '../services/auth.service';
import { RegisterPage } from '../pages/login/register.page';
import { DfDisplayComponent } from '../pages/stats/df-display.component';
import { DfSetupService } from '../pages/setup/df-setup.service';
import { HeadComponent } from '../components/misc/header.component';
import { MessageComponent } from '../components/feedback/message.component';
import { StatsPage } from '../pages/stats/stats';
import { TruncatePipe } from '../pipes/truncate.pipe';
import { MistComponent } from '../components/mist/mist';
import { TrackerComponent } from '../components/trackers/tracker';
import { InGamePage } from '../pages/start-game/in-game';
import { DfEditComponent } from '../components/df-list/df-edit';
import { DfItemComponent } from '../components/df-list/df-item';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage/es2015';

import { SetupTabsPage } from '../pages/setup/setup-tabs';
import { StartGamePage } from '../pages/start-game/start-game';
import { SettingsPage } from '../pages/settings/settings';
import { ScorePage } from '../pages/score/score';
import { HistoryPage } from '../pages/history/history';
import { HomePage } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";

import { DfFormComponent } from '../components/df-inputs/df-form.component';
import { DfInputComponent } from '../components/df-inputs/df-input.component';
import { DfListComponent } from '../components/df-list/df-list';

import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { RestProvider } from '../providers/rest.provider';
import { TeamListPage } from "../pages/setup/team-list/team-list";
import { TrackerListPage } from "../pages/setup/match-list/match-list";
import { PlayerListPage } from '../pages/setup/player-list/player-list';
import { DragulaModule } from 'ng2-dragula';
import { TrackersComponent } from "../components/trackers/trackers";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    DfInputComponent,
    DfFormComponent,
    DfListComponent,
    HomePage,
    DfItemComponent,
    HistoryPage,
    ScorePage,
    SettingsPage,
    StartGamePage,
    SetupTabsPage,
    PlayerListPage,
    TeamListPage,
    TrackerListPage,
    DfEditComponent,
    InGamePage,
    TrackersComponent,
    TrackerComponent,
    MistComponent,
    TruncatePipe,
    StatsPage,
    MessageComponent,
    HeadComponent,
    DfDisplayComponent,
    RegisterPage
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    HttpModule,
    DragulaModule,    
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
   }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule
   
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    HistoryPage,
    DfItemComponent,
    ScorePage,
    SettingsPage,
    StartGamePage,
    SetupTabsPage,
    PlayerListPage,
    TeamListPage,
    TrackerListPage,
    InGamePage,
    StatsPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DfSetupService,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LocalStorageProvider,
    RestProvider,
    AngularFireDatabase
  ]
})
export class AppModule {}
